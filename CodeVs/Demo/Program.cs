﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using CodeVsDAL;
using CodeVsDAL.AccountManagers;
using CodeVsModels.Entities;
using CodeVsServices.Services;
using CodeVsServices.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using SimpleLang;

namespace Demo
{
    class Program
    {
        private static void Main(string[] args)
        {
            var xsdMarkup = File.ReadAllText(@"..\..\Schema.xsd");
            var schemas = new XmlSchemaSet();
            schemas.Add("", XmlReader.Create(new StringReader(xsdMarkup)));

            foreach (string fileName in Directory.GetFiles(@"..\..\cpp_files"))
            {
                var xdoc = LanguageParser.GetXDocumentFromFile(fileName);
                var saveName = String.Format("{0}.xml", fileName.Split('\\').Last());
                xdoc.Save(@"..\..\xml_files\" + saveName);
                var errorList = new List<string>();
                xdoc.Validate(schemas, (sender, eventArgs) => errorList.Add(eventArgs.Message));
                Console.WriteLine(!errorList.Any() ? "Document is valid" : "Document invalid" + fileName);
            }

            var code = File.ReadAllText(@"..\..\cpp_files\1");
            var codeXdoc = LanguageParser.GetXDocument(code);

            var ss = new SolutionService();
            var ts = new TaskService();
            var aa = new AttachFileService();

            //var tt = aa.Get().ToList();
            //var ttt = ts.Get().ToList();
            //var tttt = ss.Get().ToList();

            //var t = aa.Get(x => x.TaskId == 1);
            //var t1 = aa.GetSingle(x => x.Description.Contains("sadssa"));
            //aa.Delete(1);
            //aa.Save();
            //aa.Update();

            var userManager = new CodeVsUserManager(new UserStore<User>(new CodeVsContext()));

            var newUser = new User { UserName = "login", Email = "login@gmail.com" };

            var user = userManager.FindByName("login");

            if (user == null)
            {
                var result = userManager.Create(newUser, "Test1234_");
                user = userManager.FindByName("login");
            }

            byte[] fileBytes = Encoding.ASCII.GetBytes("aaaaaaaaa");

            var afile = new AttachFileModel()
            {
                Name = "asdsad",
                Extension = ".zip",
                Content = fileBytes,
                CreateUser = user.UserName,
                Description = "Attach File Description",
            };

            var task = new TaskModel()
            {
                Title = "Task1",
                Description = "Description",
                DetailDescription = "DetailDescription",
                CreateUser = user.UserName,
                Attach = new[] { afile }
            };

            var sfile = new SolutionFileModel()
            {
                Name = "asdsad",
                Content = "bbbbbbbb",
                Extension = ".cpp",
                CreateUser = user.UserName,
            };

            var sfile1 = new SolutionFileModel()
            {
                Name = "asdsad",
                Content = "bbbbbbbb",
                Extension = ".cpp",
                CreateUser = user.UserName,
            };

            var solution = new SolutionModel()
            {
                SolutionFiles = new[] { sfile },
                CreateUser = user.UserName,
            };

            var solution1 = new SolutionModel()
            {
                SolutionFiles = new[] { sfile1 },
                CreateUser = user.UserName,
            };

            task.Solutions = new[] { solution, solution1 };

            try
            {
                ts.Save(task);
            }
            catch (Exception ex)
            {
                throw;
            }

            Console.ReadKey();
        }
    }
}
