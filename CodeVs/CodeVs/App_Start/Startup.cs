﻿using CodeVsDAL;
using CodeVsDAL.AccountManagers;
using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;

namespace CodeVs
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.CreatePerOwinContext<CodeVsContext>(CodeVsContext.Create);
            app.CreatePerOwinContext<CodeVsUserManager>(CodeVsUserManager.Create);

            // регистрация менеджера ролей
            app.CreatePerOwinContext<CodeVsRoleManager>(CodeVsRoleManager.Create);

            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/Login"),
            });
        }
    }
}