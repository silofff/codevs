﻿#region using

using System;
using System.Collections.Generic;
using System.Linq;

#endregion

namespace CodeVsModels.Interfaces {
    /// <summary>
    /// Interface for providing data access capabilities
    /// </summary>
    public interface IRepository<T> : IQueryable<T> where T : class, IEntity {
        /// <summary>
        /// Gets entity by id
        /// </summary>
        /// <param name="id">id of entity</param>
        /// <returns>entity, if found by id</returns>
        T GetById(object id);

        /// <summary>
        /// Saves new object to database
        /// </summary>
        /// <param name="t">object to save</param>
        /// <returns>saved instance</returns>
        T Save(T t);

        //
        IEnumerable<T> Save(IEnumerable<T> entities);

        /// <summary>
        /// Updates object in database
        /// </summary>
        /// <param name="t">object to update</param>
        T Update(T t);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="updateAction"></param>
        /// <returns></returns>
        T Update(object id, Action<T> updateAction);

        /// <summary>
        /// Deletes object from database
        /// </summary>
        /// <param name="id">id of object to delete</param>
        void Delete(object id);

        // Delete entity
        T Delete(T entity);

        // Delete entities
        IEnumerable<T> Delete(IEnumerable<T> entities);
    }

}
