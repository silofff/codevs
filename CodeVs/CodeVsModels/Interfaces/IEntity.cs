﻿#region Using

using System;

#endregion


namespace CodeVsModels.Interfaces {
    public interface IEntity {
        /// <summary>
        ///		Key
        /// </summary>
        int Id { get; set; }

        /// <summary>
        ///     Date, when object was created
        /// </summary>
        DateTime CreateDate { get; set; }

        /// <summary>
        ///     login of user, who created object
        /// </summary>
        string CreateUser { get; set; }
    }

}
