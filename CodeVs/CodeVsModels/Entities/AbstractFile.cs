﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeVsModels.Entities
{
    public abstract class AbstractFile : AbstractEntity
    {
        #region Properties
        //
        [Required]
        [StringLength(AbstractFile.Schema.NameLength)]
        public string Name { get; set; }

        [Required]
        [StringLength(AbstractFile.Schema.ExtensionLength)]
        public string Extension { get; set; }

        [Required]
        public byte[] Content { get; set; }

        #endregion

        #region Schema
        //
        public static class Schema
        {
            public const int NameLength = 255;
            public const int DescriptionLength = 300;
            public const int ExtensionLength = 8;
        }
        #endregion

    }
}

