﻿#region using

using System.ComponentModel.DataAnnotations;

#endregion

namespace CodeVsModels.Entities {
    public class GroupUser : AbstractEntity {
        #region properties
        [Required]
        public int GroupId { get; set; }
        public Group Group { get; set; }
        [Required]
        public string UserId { get; set; }
        public User User { get; set; }
        #endregion
    }
}
