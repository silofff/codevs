﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CodeVsModels.Interfaces;

namespace CodeVsModels.Entities
{
    public abstract class AbstractEntity : IEntity
    {
        #region Properties
        [Required]
        [Index(IsUnique = true)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public virtual int Id { get; set; }

        /// <summary>
        ///     Date, when object was created
        /// </summary>
        public DateTime CreateDate { get; set; }

        /// <summary>
        ///     login of user, who created object
        /// </summary>
        public string CreateUser { get; set; }

        #endregion
    }
}
