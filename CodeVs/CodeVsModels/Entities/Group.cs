﻿#region using

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;

#endregion

namespace CodeVsModels.Entities {
    public class Group : AbstractEntity {
        #region Properties
        [Required]
        [StringLength(Schema.NameLength)]
        public string Name { get; set; }
        public ICollection<GroupUser> UserGroups { get; set; }


        #endregion

        public class Schema 
        {
            public const int NameLength = 100;
        }

    }
}
