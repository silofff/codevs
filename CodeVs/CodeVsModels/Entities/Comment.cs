﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeVsModels.Entities
{
    public class Comment : AbstractEntity
    {
        #region Properties
        //
        [Required]
        [StringLength(Schema.MessageLength)]
        public string Message { get; set; }

        #endregion

        #region Schema
        //
        public static class Schema
        {
            public const int MessageLength = 300;
        }
        #endregion
    }
}
