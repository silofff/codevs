﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeVsModels.Entities
{
    [Table("Solutions")]
    public class Solution : AbstractEntity
    {
        #region Properties
        //
        [Required]
        public int TaskId { get; set; }
        [ForeignKey("TaskId")]
        public Task Task { get; set; }

        [InverseProperty("Solution")]
        public ICollection<SolutionFile> SolutionFiles { get; set; }

        #endregion

        #region Schema
        //
        public static class Schema
        {
        }
        #endregion

    }
}
