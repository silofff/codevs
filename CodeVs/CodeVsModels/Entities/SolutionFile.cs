﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;

namespace CodeVsModels.Entities
{
    public class SolutionFile : AbstractFile
    {
        [Required]
        public int SolutionId { get; set; }
        [ForeignKey("SolutionId")]
        public Solution Solution { get; set; }
    }
}
