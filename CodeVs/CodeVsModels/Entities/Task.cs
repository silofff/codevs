﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;

namespace CodeVsModels.Entities
{
    [Table("Tasks")]
    public class Task : AbstractEntity {
        
        #region Properties
        //
        [Required]
        [StringLength(Schema.TitleLength)]
        public string Title { get; set; }

        [Required]
        [StringLength(Schema.DescriptionLength)]
        public string Description { get; set; }

        public string DetailDescription { get; set; }

        [InverseProperty("Task")]
        public ICollection<AttachFile> Attach { get; set; }

        [InverseProperty("Task")]
        public ICollection<Solution> Solutions { get; set; }

     	#endregion

		#region Schema
		//
		public static class Schema {
            public const int TitleLength = 100;
            public const int DescriptionLength = 300;
			public const int UrlLength = 255;
		}
		#endregion

    }
}
