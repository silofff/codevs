﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Identity.EntityFramework;

namespace CodeVsModels.Entities
{
    public class Role : IdentityRole
    {
        public Role() { }
        public string Description { get; set; }

        public class Schema
        {
            public const int NameLength = 50;
        }
    }
}