﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNet.Identity.EntityFramework;

namespace CodeVsModels.Entities
{
    public class User : IdentityUser
    {
        [StringLength(Schema.FirstNameLength)]
        public string FirstName { get; set; }

        [StringLength(Schema.LastNameLength)]
        public string LastName { get; set; }
        public string ImageUrl { get; set; }

        public ICollection<GroupUser> UserGroups { get; set; }

        public User()
        {
        }
        public class Schema
        {
            public const int FirstNameLength = 50;
            public const int LastNameLength = 50;
        }
    }
}