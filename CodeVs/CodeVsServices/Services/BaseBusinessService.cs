﻿#region Using
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using AutoMapper;
using CodeVsDAL;
using CodeVsModels.Entities;
using CodeVsModels.Interfaces;
using CodeVsServices.Interfaces;

#endregion

namespace CodeVsServices.Services {
    public abstract class BaseBusinessService : IBusinessService {
        #region Properties

        public CodeVsContext Context { get; set; }

        #endregion

        #region ctor
        protected BaseBusinessService()
        {
            Context = CodeVsContext.Create();
            if (!Mapper.GetAllTypeMaps().Any()) {
                MapperConfig.Configure();
            }
        }
        #endregion

        #region Methods

        protected int SaveChanges() {
            return Context.SaveChanges();
        }

        #endregion
    }

    public abstract class BaseBusinessService<T, U> : BaseBusinessService, IBusinessService<T, U> where T : class, IEntity where U : class, IViewModel {

        #region Properties
        #endregion

        #region ctor
        #endregion

        #region private methods
        protected T Save(T t)
        {
            T newEnt = Context.Set<T>().Add(t);

            SaveChanges();
            return newEnt;
        }

        //
        protected T Update(T t) {
            Context.Entry(t).State = EntityState.Modified;
            SaveChanges();
            return t;
        }

        //
        protected T Update(int id, Action<T> updateAction) {
            if (updateAction == null) throw new ArgumentNullException("updateAction");
            var entity = this.Context.Set<T>().FirstOrDefault(x=>x.Id == id);
            if (entity != null) {
                updateAction(entity);
                SaveChanges();
            }
            return entity;
        }


        #endregion

        #region Methods IBusinessService

        protected IQueryable<T> GetQuery(Expression<Func<T, bool>> predicate, IEnumerable<string> includeList = null) {
            var query = Context.Set<T>().AsQueryable();
            if (includeList != null) {
                foreach (var item in includeList) {
                    query = query.Include(item);
                }
            }
            if (predicate != null) {
                query = query.Where(predicate);
            }
            return query;
        }

        public virtual IEnumerable<U> Get(IEnumerable<string> includeList = null) {
            return Get(null, includeList);
        }

        public virtual IEnumerable<U> Get(Expression<Func<T, bool>> predicate, IEnumerable<string> includeList = null) {
            var query = GetQuery(predicate, includeList);
            return query.ToList().Select(Mapper.Map<T, U>).AsEnumerable();
        }

        public virtual U GetSingle(Expression<Func<T, bool>> predicate, IEnumerable<string> includeList = null) {
            var query = GetQuery(predicate, includeList);
            var result = query.FirstOrDefault();
            return result != null ? Mapper.Map<T, U>(result) : null;
        }

        public virtual U Save(U u) {
            var dbItem = Mapper.Map<U, T>(u);
            return Mapper.Map<T, U>(Save(dbItem));
        }


        public virtual U Update(U u) {
            var mapped = Mapper.Map<U, T>(u);
            Action<T> updateAction = dbItem => Mapper.Map<T, T>(mapped, dbItem);
            return Mapper.Map<T, U>(Update(mapped.Id, updateAction));
        }

        public virtual void Delete(object id) {
            var set = Context.Set<T>();
            T ent = set.Find(id);
            if (ent != null) {
                set.Remove(ent);
                SaveChanges();
            }
        }
        #endregion
    }
}
