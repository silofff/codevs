﻿#region using

using System.Collections.ObjectModel;
using System.Linq;
using CodeVsModels.Entities;
using CodeVsServices.Interfaces;
using CodeVsServices.ViewModels;

#endregion

namespace CodeVsServices.Services {
    public class GroupService : BaseBusinessService<Group, GroupModel>, IGroupService {
    }
}
