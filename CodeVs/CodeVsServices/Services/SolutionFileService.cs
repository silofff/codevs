﻿#region using

using System.Data.Entity.Core.Common.CommandTrees;
using CodeVsDAL;
using CodeVsModels.Entities;
using CodeVsServices.Interfaces;
using CodeVsServices.ViewModels;

#endregion

namespace CodeVsServices.Services {
    public class SolutionFileService : BaseBusinessService<SolutionFile, SolutionFileModel>, ISolutionFileService
    {
        public void SaveBinaryFile(SolutionFileModel fileModel, byte[] fileContent)
        {
            fileModel.Content = System.Text.Encoding.Default.GetString(fileContent);
            Save(fileModel);
        }
    }
}
