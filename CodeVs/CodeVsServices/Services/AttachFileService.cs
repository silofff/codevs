﻿#region using

using CodeVsModels.Entities;
using CodeVsServices.Interfaces;
using CodeVsServices.ViewModels;

#endregion

namespace CodeVsServices.Services {
    public class AttachFileService : BaseBusinessService<AttachFile, AttachFileModel>, IAttachFileService
    {
    }
}
