﻿#region using
using CodeVsServices.Interfaces;
using CodeVsServices.ViewModels;
using CodeVsModels.Entities;

#endregion

namespace CodeVsServices.Services
{
    public class GroupUserService : BaseBusinessService<GroupUser, GroupUserModel>, IGroupUserService
    {
    }
}
