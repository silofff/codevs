﻿#region using

using System.Text.RegularExpressions;
using CodeVsModels.Entities;
using CodeVsServices.ViewModels;

#endregion

namespace CodeVsServices.Interfaces {
    public interface ISolutionService : IBusinessService<Solution, SolutionModel> {

    }
}
