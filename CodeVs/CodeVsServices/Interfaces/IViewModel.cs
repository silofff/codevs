﻿#region using

using System;

#endregion

namespace CodeVsServices.Interfaces {
    public interface IViewModel {
        /// <summary>
        ///		Key
        /// </summary>
        int Id { get; set; }

        /// <summary>
        ///     Date, when object was created
        /// </summary>
        DateTime CreateDate { get; set; }

        /// <summary>
        ///     login of user, who created object
        /// </summary>
        string CreateUser { get; set; }
    }
}
