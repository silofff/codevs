﻿#region using

using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using CodeVsModels.Interfaces;

#endregion

namespace CodeVsServices.Interfaces {
    public interface IBusinessService {
    }

    //
    public interface IBusinessService<T, U> where T : class, IEntity where U : class, IViewModel {
        IEnumerable<U> Get(IEnumerable<string> includeList = null);
        IEnumerable<U> Get(Expression<Func<T, bool>> predicate, IEnumerable<string> includeList = null);

        U GetSingle(Expression<Func<T, bool>> predicate, IEnumerable<string> includeList = null);

        U Save(U u);

        U Update(U t);

        void Delete(object id);
    }
}
