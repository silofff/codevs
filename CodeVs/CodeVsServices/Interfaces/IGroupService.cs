﻿#region using
using CodeVsModels.Entities;
using CodeVsServices.ViewModels;

#endregion

namespace CodeVsServices.Interfaces {
    public interface IGroupService : IBusinessService<Group , GroupModel>
    {

    }
}
