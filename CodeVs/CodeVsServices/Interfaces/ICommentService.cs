﻿#region using

using System.Text.RegularExpressions;
using CodeVsModels.Entities;
using CodeVsServices.ViewModels;

#endregion

namespace CodeVsServices.Interfaces {
    public interface ICommentService : IBusinessService<Comment, CommentModel> {

    }
}
