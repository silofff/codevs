﻿#region using

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

#endregion

namespace CodeVsServices.ViewModels {


    public class GroupModel : AbstractModel {
        #region Properties

        [Required]
        public string Name { get; set; }

        public ICollection<GroupUserModel> GroupUsers { get; set; }
        public bool Selected { get; set; }
        #endregion
    }
}
