﻿#region using

using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using CodeVsModels.Entities;

#endregion

namespace CodeVsServices.ViewModels {

    public class AttachFileModel : AbstractModel {
        #region Properties

        [Required]
        [StringLength(AbstractFile.Schema.NameLength)]
        public string Name { get; set; }
        [Required]
        public byte[] Content { get; set; }
        [Required]
        [StringLength(AbstractFile.Schema.ExtensionLength)]
        public string Extension { get; set; }
        public int TaskId { get; set; }
        public string Description { get; set; }

        #endregion
    }
}
