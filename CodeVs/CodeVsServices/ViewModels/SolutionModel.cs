﻿#region Using

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

#endregion

namespace CodeVsServices.ViewModels {
    public class SolutionModel : AbstractModel
    {
        #region Properties
        //
        public int TaskId { get; set; }
        public ICollection<SolutionFileModel> SolutionFiles { get; set; }

        #endregion 
    }

}
