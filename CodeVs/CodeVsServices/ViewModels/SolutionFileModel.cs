﻿#region using

using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using CodeVsModels.Entities;

#endregion

namespace CodeVsServices.ViewModels {

    public class SolutionFileModel : AbstractModel {
        #region Properties

        [Required]
        [StringLength(AbstractFile.Schema.NameLength)]
        public string Name { get; set; }
        public string Content { get; set; }

        [Required]
        [StringLength(AbstractFile.Schema.ExtensionLength)]
        public string Extension { get; set; }
        public int SolutionId { get; set; }

        #endregion
    }
}
