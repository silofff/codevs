﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CodeVsModels.Entities;
using Task = CodeVsModels.Entities.Task;

namespace CodeVsServices.ViewModels
{
   public class TaskModel : AbstractModel
    {
        #region Properties
        //
        [Required]
        [StringLength(Task.Schema.TitleLength)]
        public string Title { get; set; }
        //
        [Required]
        [StringLength(Task.Schema.DescriptionLength)]
        public string Description { get; set; }

        public string DetailDescription { get; set; }

        public ICollection<AttachFileModel> Attach { get; set; }
        public ICollection<SolutionModel> Solutions { get; set; }

        #endregion
    }
}
