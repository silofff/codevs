﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CodeVsModels.Entities;
using Task = CodeVsModels.Entities.Task;

namespace CodeVsServices.ViewModels
{
   public class CommentModel : AbstractModel
    {
        #region Properties
        //
        [Required]
        public string Message { get; set; }
        #endregion
    }
}
