﻿#region Using

using System;
using CodeVsServices.Interfaces;

#endregion

namespace CodeVsServices.ViewModels {
    public abstract class AbstractModel: IViewModel {
        protected AbstractModel()
        {
            CreateDate = DateTime.Now;
        }

        #region Properties IViewModel
        public virtual int Id { get; set; }

        /// <summary>
        ///     Date, when object was created
        /// </summary>
        public DateTime CreateDate {get; set;}

        /// <summary>
        ///     login of user, who created object
        /// </summary>
        public string CreateUser { get; set; }
        #endregion
    }
}
