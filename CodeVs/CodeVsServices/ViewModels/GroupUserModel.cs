﻿#region using

using System.ComponentModel.DataAnnotations;

#endregion

namespace CodeVsServices.ViewModels {

    public class GroupUserModel : AbstractModel {
        #region Properties

        [Required]
        public int GroupId { get; set; }
        [Required]
        public string UserId { get; set; }
        #endregion
    }
}
