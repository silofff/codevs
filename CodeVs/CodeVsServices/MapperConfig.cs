﻿#region using
using AutoMapper;

using CodeVsModels.Entities;
using CodeVsServices.ViewModels;

#endregion

namespace CodeVsServices
{

    public static class MapperExtention
    {
        public static IMappingExpression<TSource, TDestination> SkipCreateChangeMapping<TSource, TDestination>(this IMappingExpression<TSource, TDestination> mappingExpression)
            where TSource : AbstractEntity
            where TDestination : AbstractEntity
        {
            mappingExpression.ForMember(src => src.CreateUser, dest => dest.Ignore())
                .ForMember(src => src.CreateDate, dest => dest.Ignore());
            return mappingExpression;
        }
    }

    public class MapperConfig
    {
        public static void Configure()
        {
            Mapper.CreateMap<SolutionFile, SolutionFile>().SkipCreateChangeMapping();
            Mapper.CreateMap<SolutionFile, SolutionFileModel>();
            Mapper.CreateMap<SolutionFile, SolutionFileModel>()
                .ForMember(src => src.Content, dest => dest.MapFrom(src => System.Text.Encoding.Default.GetString(src.Content)));
            Mapper.CreateMap<SolutionFileModel, SolutionFile>().ReverseMap();

            Mapper.CreateMap<AttachFile, AttachFile>().SkipCreateChangeMapping();
            Mapper.CreateMap<AttachFile, AttachFileModel>();
            Mapper.CreateMap<AttachFileModel, AttachFile>().ReverseMap();

            Mapper.CreateMap<Solution, Solution>().SkipCreateChangeMapping();
            Mapper.CreateMap<Solution, SolutionModel>();
            Mapper.CreateMap<SolutionModel, Solution>().ReverseMap();

            Mapper.CreateMap<Task, Task>().SkipCreateChangeMapping();
            Mapper.CreateMap<Task, TaskModel>();
            Mapper.CreateMap<TaskModel, Task>().ReverseMap();

            Mapper.CreateMap<Comment, Comment>().SkipCreateChangeMapping();
            Mapper.CreateMap<Comment, CommentModel>();
            Mapper.CreateMap<CommentModel, Comment>().ReverseMap();

            Mapper.CreateMap<Group, Group>().SkipCreateChangeMapping();
            Mapper.CreateMap<Group, GroupModel>();
            Mapper.CreateMap<GroupModel, Group>().ReverseMap();

            Mapper.CreateMap<GroupUser, GroupUser>().SkipCreateChangeMapping();
            Mapper.CreateMap<GroupUser, GroupUserModel>();
            Mapper.CreateMap<GroupUserModel, GroupUser>().ReverseMap();
        }
    }
}
