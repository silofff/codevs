﻿using System;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Validation;
using CodeVsModels.Entities;
using CodeVsModels.Interfaces;
using Microsoft.AspNet.Identity.EntityFramework;

namespace CodeVsDAL
{
    public class CodeVsContext : IdentityDbContext<User>
    {
        public DbSet<GroupUser> GroupUsers { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Task> Tasks { get; set; }
        public DbSet<Solution> Solutions { get; set; }
        public DbSet<SolutionFile> SolutionFiles { get; set; }
        public DbSet<AttachFile> AttachFiles { get; set; }

        public CodeVsContext() : base("CodeVsContext")
        {
            //Comment this line for non-console application
            AppDomain.CurrentDomain.SetData("DataDirectory", System.IO.Directory.GetCurrentDirectory());
        }

        public static CodeVsContext Create()
        {
            return new CodeVsContext();
        }

    }
}