﻿using CodeVsModels.Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;

namespace CodeVsDAL.AccountManagers
{
    public class CodeVsUserManager : UserManager<User>
    {
        public CodeVsUserManager(IUserStore<User> store)
            : base(store)
        {
        }
        public static CodeVsUserManager Create(IdentityFactoryOptions<CodeVsUserManager> options,
                                            IOwinContext context)
        {
            var db = context.Get<CodeVsContext>();
            var manager = new CodeVsUserManager(new UserStore<User>(db));
            return manager;
        }
    }
}