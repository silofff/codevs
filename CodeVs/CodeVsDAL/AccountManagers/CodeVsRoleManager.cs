﻿using CodeVsModels.Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;

namespace CodeVsDAL.AccountManagers
{
    public class CodeVsRoleManager : RoleManager<Role>
    {
        public CodeVsRoleManager(RoleStore<Role> store)
            : base(store)
        {
            
        }
        public static CodeVsRoleManager Create(IdentityFactoryOptions<CodeVsRoleManager> options,
                                           IOwinContext context)
        {
            return new CodeVsRoleManager(new
                    RoleStore<Role>(context.Get<CodeVsContext>()));
        }
    }
}