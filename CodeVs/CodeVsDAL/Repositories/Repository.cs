﻿#region Using
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using CodeVsModels.Interfaces;

#endregion

namespace CodeVsDAL.Repositories {
    /// <summary>
    /// Repository implementation
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Repository<T> : IRepository<T> where T : class, IEntity {
        #region Fields

        private readonly CodeVsContext m_dbContext;
        private readonly DbSet<T> m_entitySet;

        #endregion

        #region Ctor

        //
        public Repository(CodeVsContext dbContext)
        {
            if (dbContext == null) throw new ArgumentNullException("dbContext");
            this.m_dbContext = dbContext;
            this.m_entitySet = dbContext.Set<T>();
        }

        #endregion

        #region Properties

        //
        protected CodeVsContext Context {
            get { return this.m_dbContext; }
        }

        //
        protected DbSet<T> EntitySet {
            get { return this.m_entitySet; }
        }

        #endregion

        #region Methods

        //

        public T GetById(object id) {
            return this.EntitySet.Find(id);
        }

        //
        public T Save(T t) {
            T newEnt = this.EntitySet.Add(t);
            this.SaveChanges();
            return newEnt;
        }

        //
        public IEnumerable<T> Save(IEnumerable<T> entities) {
            if (entities == null) throw new ArgumentNullException();
            entities = this.EntitySet.AddRange(entities is ICollection<T> ? entities : entities.ToList());
            this.SaveChanges();
            return entities;
        }

        //
        public T Update(T t) {
            this.Context.Entry(t).State = EntityState.Modified;
            this.SaveChanges();
            return t;
        }

        //
        public T Update(object id, Action<T> updateAction) {
            if (updateAction == null) throw new ArgumentNullException("updateAction");
            var entity = this.EntitySet.Find(id);
            if (entity != null) {
                updateAction(entity);
                this.SaveChanges();
            }
            return entity;
        }

        //
        public void Delete(object id) {
            var set = this.EntitySet;
            T ent = set.Find(id);
            if (ent != null) {
                set.Remove(ent);
                this.SaveChanges();
            }
        }

        // Delete entity
        public T Delete(T entity) {
            if (entity == null) throw new ArgumentNullException("entity");
            entity = this.EntitySet.Remove(entity);
            this.SaveChanges();
            return entity;
        }

        // Delete entities
        public IEnumerable<T> Delete(IEnumerable<T> entities) {
            if (entities == null) throw new ArgumentNullException("entities");
            entities = this.EntitySet.RemoveRange(entities is ICollection<T> ? entities : entities.ToList());
            this.SaveChanges();
            return entities;
        }

        //
        protected int SaveChanges() {
            //var principal = this.Principal;
            //return this.Context.SaveChanges(principal == null ? (int?)null : principal.UserId);
            return this.Context.SaveChanges();
        }

        #endregion

        #region IQueryable<T> implementation

        //
        IEnumerator<T> IEnumerable<T>.GetEnumerator() {
            return ((IEnumerable<T>)this.EntitySet).GetEnumerator();
        }

        //
        IEnumerator IEnumerable.GetEnumerator() {
            return ((IEnumerable)this.EntitySet).GetEnumerator();
        }

        //
        Expression IQueryable.Expression {
            get { return ((IQueryable)this.EntitySet).Expression; }
        }

        //
        Type IQueryable.ElementType {
            get { return ((IQueryable)this.EntitySet).ElementType; }
        }

        //
        IQueryProvider IQueryable.Provider {
            get { return ((IQueryable)this.EntitySet).Provider; }
        }

        #endregion
    }
}
