﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace ProgramTree
{
    [Serializable]
    [XmlInclude(typeof(Node))]
    public class Node  // базовый класс для всех узлов    
    {

    }

    public class ExprNode : Node // базовый класс для всех выражений
    {

    }
    public class IntNumNode : ExprNode
    {
        public int Num { get; set; }
        public IntNumNode(int num) { Num = num; }
    }

    [Serializable]
    [XmlInclude(typeof(StatementNode))]
    public class StatementNode : XElement // базовый класс для всех операторов
    {
        public StatementNode(string el, XAttribute atr)
            : base(el, atr)
        {
            
        }

        public StatementNode(string name)
            : base(name)
        {
            
        }
    }

    public class ActionNode : StatementNode
    {
        public String Body { get; set; }

        public ActionNode(string name)
            : base(name)
        {
        }
        public ActionNode(string name, string atrName, string atr)
            : base(name, new XAttribute(atrName, atr))
        {
            Body = atr;
        }
    }

    public class BlockNode : StatementNode
    {
        public List<StatementNode> StList = new List<StatementNode>();

        public BlockNode(string name, StatementNode stat)
            : base(name)
        {
            this.Add(stat);
            StList.Add(stat);
        }

        public BlockNode(string name)
            : base(name)
        {
        }
        public void AddNode(StatementNode stat)
        {
            this.Add(stat);
            StList.Add(stat);
        }
    }

}