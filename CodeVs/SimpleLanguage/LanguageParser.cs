﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using SimpleParser;
using SimpleScanner;

namespace SimpleLang
{
    public static class LanguageParser
    {
        public static XDocument GetXDocumentFromFile(string path)
        {
            try
            {
                string code = File.ReadAllText(path);
                return GetXDocument(code);
            }
            catch (FileNotFoundException)
            {
                throw new FileNotFoundException(String.Format("Файл {0} не найден", path));
            }
        }

        public static XDocument GetXDocument(string code)
        {
            XDocument xdoc = null;
            try
            {
                var scanner = new Scanner();
                scanner.SetSource(CleanString(code), 0);

                var parser = new Parser(scanner);

                var b = parser.Parse();
                if (!b)
                    Console.WriteLine("Ошибка");
                else
                {
                    Console.WriteLine("Синтаксическое дерево построено");
                    xdoc = new XDocument();
                    xdoc.Add(parser.root);
                }
            }
            catch (LexException e)
            {
                throw new LexException("Лексическая ошибка. " + e.Message);
            }
            catch (SyntaxException e)
            {
                throw new SyntaxException("Синтаксическая ошибка. " + e.Message);
            }
            return xdoc;
        }

        static string CleanString(string code)
        {
            var trimmer = new Regex(@"\t+| +");
            code = trimmer.Replace(code, " ");

            code = code.Replace("{", "\r\n{\r\n");
            code = code.Replace("}", "\r\n}\r\n");

            var trimmer1 = new Regex(@"\r\n\s+");
            code = trimmer1.Replace(code, "\r\n");

            return code;
        }
    }
}
