%using SimpleParser;
%using QUT.Gppg;
%using System.Linq;

%namespace SimpleScanner

Alpha 	[a-zA-Z_]
Digit   [0-9] 
AlphaDigit {Alpha}|{Digit}
assign =
semicolon ;
if if
def #define
incl #include
clause \(.+\)
package <(.+)>
us using
ws [ \t\n\r\f\v]
COMMENT1 \/\/.*
COMMENT2 \/\*.*\*\/
COMMENT {COMMENT1}|{COMMENT2}

ACTION .+{semicolon}{ws}*

BRACE [\{\}]
CONDITION {ws}*{if}{ws}*{clause}{ws}*
INTNUM  {Digit}+
REALNUM {INTNUM}\.{INTNUM}
ID \**{Alpha}{AlphaDigit}*
FUNC {ID}{ws}*{clause}{semicolon}


DEFINE {def}{ws}*{ID}
INCLUDE {incl}{ws}*{package}
USING {us}{ACTION}


RETURN return{ACTION}

TYPE int|FILE|void|double
DECLARATION {TYPE}{ACTION}

MAIN {ws}*{TYPE}{ws}*main{ws}*\(.*\){ws}*

FOR {ws}*for{ws}*{clause}
WHILE {ws}*while{ws}*{clause}
DO do

%%
{MAIN} { 
   yylval.sVal = yytext; 
  return (int)Tokens.MAIN; 
}

{DO} { 
  return (int)Tokens.DO; 
}

{FOR} { 
   yylval.sVal = yytext; 
  return (int)Tokens.FOR; 
}

{WHILE} { 
  yylval.sVal = yytext; 
  return (int)Tokens.WHILE; 
}

{TYPE} { 
 // return (int)Tokens.TYPE; 
}

{DECLARATION} { 
   yylval.sVal = yytext; 
  return (int)Tokens.DECLARATION; 
}

{RETURN} { 
   yylval.sVal = yytext; 
  return (int)Tokens.RETURN; 
}

{COMMENT} { 
   //yylval.sVal = yytext; 
  //return (int)Tokens.COMMENT; 
}

{FUNC} { 
  yylval.sVal = yytext; 
  return (int)Tokens.FUNC; 
}

{USING} { 
 return (int)Tokens.USING; 
}

{DEFINE} { 
 return (int)Tokens.DEFINE; 
}

{INCLUDE} { 
  return (int)Tokens.INCLUDE; 
}

{BRACE} { 
  return (int)Tokens.BRACE; 
}

{CONDITION} { 
  yylval.sVal = yytext; 
  return (int)Tokens.CONDITION; 
}

{INTNUM} { 
  yylval.iVal = int.Parse(yytext); 
  return (int)Tokens.INUM; 
}

{REALNUM} { 
  yylval.dVal = double.Parse(yytext); 
  return (int)Tokens.RNUM;
}

{ID}  { 
  int res = ScannerHelper.GetIDToken(yytext);
  if (res == (int)Tokens.ID)
	yylval.sVal = yytext;
  return res;
}


{ACTION} { 
  yylval.sVal = yytext; 
  return (int)Tokens.ACTION; 
}

[^ \r\n] {
	LexError();
}

%{
  yylloc = new LexLocation(tokLin, tokCol, tokELin, tokECol);
%}

%%

public override void yyerror(string format, params object[] args) // îáðàáîòêà ñèíòàêñè÷åñêèõ îøèáîê
{
  var ww = args.Skip(1).Cast<string>().ToArray();
  string errorMsg = string.Format("({0},{1}): Âñòðå÷åíî {2}, à îæèäàëîñü {3}", yyline, yycol, args[0], string.Join(" èëè ", ww));
  throw new SyntaxException(errorMsg);
}

public void LexError()
{
  string errorMsg = string.Format("({0},{1}): Íåèçâåñòíûé ñèìâîë {2}", yyline, yycol, yytext);
  throw new LexException(errorMsg);
}

class ScannerHelper 
{
  private static Dictionary<string,int> keywords;

  static ScannerHelper() 
  {
    keywords = new Dictionary<string,int>();
    keywords.Add("else",(int)Tokens.ELSE);
  }
  public static int GetIDToken(string s)
  {
	if (keywords.ContainsKey(s.ToLower()))
	  return keywords[s];
	else
      return (int)Tokens.ID;
  }
  
}
