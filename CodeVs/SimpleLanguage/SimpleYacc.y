%{
    public StatementNode root; 
    public Parser(AbstractScanner<ValueType, LexLocation> scanner) : base(scanner) { }
%}

%output = SimpleYacc.cs

%union { 
			public double dVal; 
			public int iVal; 
			public string sVal; 
			public Node nVal;
			public ExprNode eVal;
			public StatementNode stVal;
			public BlockNode blVal;
       }

%using ProgramTree;

%namespace SimpleParser

%token ASSIGN SEMICOLON TYPE BRACE ELSE INCLUDE USING MAIN COMMENT FOR DO DEFINE
%token <iVal> INUM 
%token <dVal> RNUM 
%token <sVal> ID ASSIGNEXPR ACTION FUNC DECLARATION CONDITION RETURN WHILE

%type <stVal> statement progr
%type <blVal> stlist block else condition loop

%%

progr   : instlist MAIN block {
						$$ = new ActionNode("program");
						$3.Name = "main";
					 	$$.Add($3);
					 	root = $$;
					}
		;

else 	: 	ELSE block {  $$ =  $2; $$.Name = "else";}
		|	ELSE statement {  $$ =  new BlockNode("else", $2);}
		;

condition 	:	CONDITION block else { $$ =  new BlockNode("condition"); $2.Name = "if"; $2.Add(new XAttribute("clause", $1));  $$.Add($2); $$.Add($3); }
			|	CONDITION block { $$ =  new BlockNode("condition"); $2.Name = "if"; $2.Add(new XAttribute("clause", $1));  $$.Add($2); }
			|	CONDITION statement else { $$ =  new BlockNode("condition"); var temp = new BlockNode("if", $2); temp.Add(new XAttribute("clause", $1));  $$.Add(temp); $$.Add($3); }
			|	CONDITION statement { $$ =  new BlockNode("condition"); var temp = new BlockNode("if", $2); temp.Add(new XAttribute("clause", $1));  $$.Add(temp);}
		;

loop 	:	WHILE block { $2.Name = "loop"; $$ =  $2; $$.Add(new XAttribute("clause", $1)); }
		|	FOR block { $2.Name = "loop"; $$ =  $2; $$.Add(new XAttribute("clause", $1)); }
			;

stlist	: statement
			{ 
			$$ = new BlockNode("block", $1); 
			}
		| stlist statement
			{ 
			$1.Add($2); 
			$$ = $1; 
			}
		;


statement: ACTION { $$ =  new ActionNode("action","body", $1); }
		| condition   { $$ = $1; }
		| loop   { $$ = $1; }
		| DECLARATION   { $$ =  new ActionNode("action","body", $1); }
		| FUNC { $$ =  new ActionNode("action","body", $1); }
		| RETURN { $$ =  new ActionNode("action","body", $1); }
	;

instlist	: initstate
			{ 
			}
		| instlist initstate
			{ 
			}
		;

initstate: INCLUDE {}
		| USING {}
		| ACTION {}
		| DEFINE {}
		| DECLARATION {}
	;

block	: BRACE stlist BRACE { $$ = $2;}
		;

%%